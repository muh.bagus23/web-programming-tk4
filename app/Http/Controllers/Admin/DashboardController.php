<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $sold_products = OrderItem::selectRaw("SUM(qty) as sold, products.stock, SUM((order_items.price - order_items.capital_price) * qty) as profit, order_items.*")
                                    ->join('products', 'products.id','=','order_items.product_id')
                                    ->orderByRaw('SUM((order_items.price - order_items.capital_price) * qty) DESC')->groupBy('product_id')->simplePaginate(20);

        $type_products = OrderItem::selectRaw("products.type, SUM(qty) as sold, products.stock, SUM((price - order_items.capital_price) * qty) as profit")
                                    ->join('products', 'products.id','=','order_items.product_id')
                                    ->orderByRaw('SUM((order_items.price - order_items.capital_price) * qty) DESC')
                                    ->groupBy('products.type')
                                    ->get();
        $type_products_label = $type_products->pluck('type');
        $type_products_data = $type_products->pluck('profit');

        $item_products = OrderItem::selectRaw("products.name, SUM(qty) as sold, products.stock, SUM((price - order_items.capital_price) * qty) as profit")
                                    ->join('products', 'products.id','=','order_items.product_id')
                                    ->orderByRaw('SUM((order_items.price - order_items.capital_price) * qty) DESC')
                                    ->groupBy('products.id')
                                    ->get();
        $item_products_label = $item_products->pluck('name');
        $item_products_data = $item_products->pluck('profit');

        $amount_products = OrderItem::selectRaw("products.name, SUM(qty) as sold, products.stock, SUM((price - order_items.capital_price) * qty) as profit")
                                    ->join('products', 'products.id','=','order_items.product_id')
                                    ->orderByRaw('SUM((order_items.price - order_items.capital_price) * qty) DESC')
                                    ->groupBy('products.id')
                                    ->get();
        $amount_products_label = $amount_products->pluck('name');
        $amount_products_data = $amount_products->pluck('sold');
        
        return view('admins.dashboard', [
            'sold_products' => $sold_products,
            'type_products_data' => $type_products_data,
            'type_products_label' => $type_products_label,
            'item_products_data' => $item_products_data,
            'item_products_label' => $item_products_label,
            'amount_products_data' => $amount_products_data,
            'amount_products_label' => $amount_products_label
        ]);
    }
}
