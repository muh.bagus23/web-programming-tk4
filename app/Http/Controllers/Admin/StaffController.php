<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    public function index()
    {
        $users = User::where('role', 'staff')->simplePaginate(10);

        return view('admins.staffs.index', [
            'users' => $users
        ]);
    }

    public function detail(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        if (!$user) {
            abort('404', 'User not found');
        }

        return view('admins.staffs.detail', [
            'user' => $user
        ]);
    }

    public function create()
    {
        return view('admins.staffs.form');
    }
    
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => ['required'],
            "birthdate" => ['required'],
            "email" => ['required', 'unique:users,email'],
            "username" => ['required', 'unique:users,username'],
            "password" => ['required', 'confirmed'],
            "password_confirmation" => ['required']
        ]);

        if ($validate->fails()) {
            return redirect()->route('admin.staff.create')->withErrors($validate)->withInput();
        }

        DB::beginTransaction();
        try {
            $user = User::create([
                'email' => $request->email,
                'name' => $request->name,
                'birthdate' => $request->birthdate,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'role' => 'staff'
            ]);
            DB::commit();
    
            return redirect()->route('admin.staff.create')->with('msg', 'Staff created successfully. <a href="'.route('admin.staff.detail', $user->id).'">Lihat Detail</a>');
        } catch (\Throwable $th) {
            DB::rollBack();
            $validate->getMessageBag()->add('system', $th->getMessage());
            
            return redirect()->route('admin.staff.create')->withErrors($validate)->withInput();
        }
    }

    public function edit(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        if (!$user) {
            abort('404', 'User not found');
        }

        return view('admins.staffs.form', [
            'user' => $user
        ]);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => ['required'],
            "birthdate" => ['required'],
            "email" => ['required'],
            "username" => ['required'],
            "password" => ['confirmed'],
            "password_confirmation" => ['required_with:password'],
        ]);

        if ($validate->fails()) {
            return redirect()->route('admin.staff.edit', $request->id)->withErrors($validate)->withInput();
        }

        DB::beginTransaction();
        try {
            $user = User::where('id', $request->id)->first();

            if ($request->password) {
                $user->update([
                    'password' => Hash::make($request->password)
                ]);
            }
    
            $user->update([
                'email' => $request->email,
                'name' => $request->name,
                'birthdate' => $request->birthdate,
                'address' => $request->address,
                'username' => $request->username,
                'password' => Hash::make($request->password)
            ]);
            DB::commit();
    
            return redirect()->route('admin.staff.edit', $user->id)->with('msg', 'Staff updated successfully. <a href="'.route('admin.staff.detail', $user->id).'">Lihat Detail</a>');
        } catch (\Throwable $th) {
            DB::rollBack();
            $validate->getMessageBag()->add('system', $th->getMessage());
            
            return redirect()->route('admin.staff.edit', $request->id)->withErrors($validate)->withInput();
        }
    }

    public function delete(Request $request)
    {
        User::where('id', $request->id)->delete();

        return redirect()->route('admin.staff.index')->with('msg', 'Staff deleted successfully.');
    }
}
