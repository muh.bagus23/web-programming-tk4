<?php

use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\StaffController as AdminStaffController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\User\DashboardController as UserDashboardController;
use App\Http\Controllers\User\OrderController as UserOrderController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false
]);

Route::prefix('admin')->name('admin.')->group(function(){
    // Admin Routes
    Route::middleware(['auth', 'role:admin'])->group(function(){
        Route::get('/', [AdminDashboardController::class, 'index'])->name('index');
    });

    // Admin & Staff Routes
    Route::middleware(['auth', 'role:admin,staff'])->group(function(){
        Route::prefix('user')->name('user.')->group(function(){
            Route::get('/', [AdminUserController::class, 'index'])->name('index');
            Route::get('/create', [AdminUserController::class, 'create'])->name('create');
            Route::post('/create', [AdminUserController::class, 'store'])->name('store');
            Route::get('/detail/{id}', [AdminUserController::class, 'detail'])->name('detail');
            Route::get('/edit/{id}', [AdminUserController::class, 'edit'])->name('edit');
            Route::put('/edit/{id}', [AdminUserController::class, 'update'])->name('update');
            Route::delete('/delete', [AdminUserController::class, 'delete'])->name('delete');
        });

        Route::prefix('staff')->name('staff.')->group(function(){
            Route::get('/', [AdminStaffController::class, 'index'])->name('index');
            Route::get('/create', [AdminStaffController::class, 'create'])->name('create');
            Route::post('/create', [AdminStaffController::class, 'store'])->name('store');
            Route::get('/detail/{id}', [AdminStaffController::class, 'detail'])->name('detail');
            Route::get('/edit/{id}', [AdminStaffController::class, 'edit'])->name('edit');
            Route::put('/edit/{id}', [AdminStaffController::class, 'update'])->name('update');
            Route::delete('/delete', [AdminStaffController::class, 'delete'])->name('delete');
        });

        Route::prefix('product')->name('product.')->group(function(){
            Route::get('/', [AdminProductController::class, 'index'])->name('index');
            Route::get('/create', [AdminProductController::class, 'create'])->name('create');
            Route::post('/create', [AdminProductController::class, 'store'])->name('store');
            Route::get('/detail/{id}', [AdminProductController::class, 'detail'])->name('detail');
            Route::get('/edit/{id}', [AdminProductController::class, 'edit'])->name('edit');
            Route::put('/edit/{id}', [AdminProductController::class, 'update'])->name('update');
            Route::delete('/delete', [AdminProductController::class, 'delete'])->name('delete');
        });

        Route::prefix('order')->name('order.')->group(function(){
            Route::get('/', [AdminOrderController::class, 'index'])->name('index');
            Route::get('/detail/{code}', [AdminOrderController::class, 'detail'])->name('detail');
            Route::post('/confirm', [AdminOrderController::class, 'confirm'])->name('confirm');
            Route::post('/reject', [AdminOrderController::class, 'reject'])->name('reject');
        });
    });
});

// User Routes
Route::middleware(['auth', 'role:user'])->group(function(){
    Route::get('/', [UserDashboardController::class, 'index'])->name('index');

    Route::prefix('order')->name('order.')->group(function(){
        Route::get('/', [UserOrderController::class, 'index'])->name('index');
        Route::get('/detail/{code}', [UserOrderController::class, 'detail'])->name('detail');
        Route::post('/add-to-cart', [UserOrderController::class, 'addToCart'])->name('addToCart');
        Route::get('/cart', [UserOrderController::class, 'cart'])->name('cart');
        Route::post('/checkout', [UserOrderController::class, 'checkout'])->name('checkout');
    });
});