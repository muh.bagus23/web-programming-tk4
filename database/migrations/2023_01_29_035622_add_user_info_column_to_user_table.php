<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('sex', 1)->nullable();
            $table->text('address')->nullable();
            $table->string('ktp_image_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('username');
            $table->dropColumn('birthdate');
            $table->dropColumn('sex', 1);
            $table->dropColumn('address');
            $table->dropColumn('ktp_image_url');
        });
    }
};
