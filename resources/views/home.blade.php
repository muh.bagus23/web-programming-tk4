@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Product Catalog') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @foreach($products as $product)
                    <div class="card" style="width: 18rem;">
                        <img src="{{ $product->product_image_url }}" class="card-img-top" alt="{{ $product->name }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p class="card-text">{{ $product->description }}</p>
                            <hr>
                            <h4>{{ 'Rp'.number_format($product->selling_price, 0, ',', '.') }}</h4>
                            <hr>
                            <form action="{{ route('order.addToCart') }}" method="POST">
                                @csrf
                                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                                <div class="d-flex">
                                    <input value="1" type="number" class="form-control text-center" style="width: 30%;" name="qty" />
                                    <button type="submit" class="ms-auto btn btn-success">Add to Cart</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
