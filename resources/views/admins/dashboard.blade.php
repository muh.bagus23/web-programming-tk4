@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-header">{{ __('Profit by Type of Product') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="typeProfitChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-header">{{ __('Profit by Product Item') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="itemProfitChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-header">{{ __('Total Product Sold of All Transaction') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="amountProfitChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Sold Product') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Product</th>
                                <th>Sold</th>
                                <th>Stock Left</th>
                                <th>Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sold_products as $key => $sold)
                            <tr>
                                <td class="text-center">{{ $sold_products->firstItem() + $key }}</td>
                                <td>{{ $sold->product->name }}</td>
                                <td>{{ $sold->sold }}</td>
                                <td>{{ $sold->stock ?? 0 }}</td>
                                <td>{{ 'Rp'.number_format($sold->profit, 0, '.', '.') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
    
                    {{ $sold_products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2"></script>
<script>
    const typeProfitChart = document.getElementById('typeProfitChart');
    new Chart(typeProfitChart, {
        type: 'pie',
        data: {
            labels: @json($type_products_label),
            datasets: [{
                label: 'Profit',
                data: @json($type_products_data),
                borderWidth: 1
            }]
        }
    });

    const itemProfitChart = document.getElementById('itemProfitChart');
    new Chart(itemProfitChart, {
        type: 'pie',
        data: {
            labels: @json($item_products_label),
            datasets: [{
                label: 'Profit',
                data: @json($item_products_data),
                borderWidth: 1
            }]
        }
    });

    const amountProfitChart = document.getElementById('amountProfitChart');
    new Chart(amountProfitChart, {
        type: 'pie',
        data: {
            labels: @json($amount_products_label),
            datasets: [{
                label: 'Total Sold',
                data: @json($amount_products_data),
                borderWidth: 1
            }]
        }
    });
</script>
@endsection