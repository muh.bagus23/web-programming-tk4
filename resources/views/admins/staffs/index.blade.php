@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <div>Staff Data</div> <a href="{{ route('admin.staff.create') }}" class="ms-auto btn btn-primary">Create</a>
                </div>
                <div class="card-body">
                    @if(session('msg'))
                    <div class="alert alert-info">
                        {!! session('msg') !!}
                    </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $key => $user)
                            <tr>
                                <td class="text-center">{{ $users->firstItem() + $key }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->username }}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-primary" href="{{ route('admin.staff.detail', $user->id) }}">Detail</a>
                                    @if(auth()->user()->isAdmin())
                                    <a class="btn btn-sm btn-warning" href="{{ route('admin.staff.edit', $user->id) }}">Edit</a>
                                    <a class="btn btn-sm btn-danger" onclick="event.preventDefault(); if(confirm('Are you sure want to delete this?')) document.getElementById('delete-form-{{ $user->id }}').submit();" href="">Delete</a>
                                    <form action="{{ route('admin.staff.delete') }}" method="POST" id="delete-form-{{ $user->id }}" style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                        <input type="hidden" value="{{ $user->id }}" name="id">
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
    
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection