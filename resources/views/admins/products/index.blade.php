@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <div>Product Data</div> <a href="{{ route('admin.product.create') }}" class="ms-auto btn btn-primary">Create</a>
                </div>
                <div class="card-body">
                    @if(session('msg'))
                    <div class="alert alert-info">
                        {!! session('msg') !!}
                    </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Product</th>
                                <th>Stock</th>
                                <th>Capital Price</th>
                                <th>Selling Price</th>
                                <th>Image</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $key => $product)
                            <tr>
                                <td class="text-center">{{ $products->firstItem() + $key }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->stock ?? 0 }}</td>
                                <td>{{ $product->capital_price }}</td>
                                <td>{{ $product->selling_price }}</td>
                                <td><img src="{{ $product->product_image_url }}" width="100px" /></td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-primary" href="{{ route('admin.product.detail', $product->id) }}">Detail</a>
                                    <a class="btn btn-sm btn-warning" href="{{ route('admin.product.edit', $product->id) }}">Edit</a>
                                    <a class="btn btn-sm btn-danger" onclick="event.preventDefault(); if(confirm('Are you sure want to delete this?')) document.getElementById('delete-form-{{ $product->id }}').submit();" href="">Delete</a>
                                    <form action="{{ route('admin.product.delete') }}" method="POST" id="delete-form-{{ $product->id }}" style="display: none;">
                                        @method('DELETE')
                                        @csrf
                                        <input type="hidden" value="{{ $product->id }}" name="id">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
    
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection