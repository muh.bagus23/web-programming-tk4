@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('My Transaction') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Invoice Number</th>
                                <th>Date</th>
                                <th>Buyer</th>
                                <th>Summary</th>
                                <th>Total Transaction</th>
                                <th>Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $key => $order)
                            <tr>
                                <td class="text-center">{{ $orders->firstItem() + $key }}</td>
                                <td>{{ $order->invoice_code }}</td>
                                <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y H:i:s') }}</td>
                                <td>{{ $order->user->name }}</td>
                                <td>{{ $order->items->count().' Product(s)' }}</td>
                                <td>{{ 'Rp'.number_format($order->total_price, 0, ',', '.') }}</td>
                                <td>
                                @if($order->status == 'waiting_confirmation')
                                Waiting Confirmation
                                @elseif($order->status == 'completed')
                                Completed
                                @elseif($order->status == 'rejected')
                                Rejected
                                @endif
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-primary" href="{{ route('admin.order.detail', $order->invoice_code) }}">Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
