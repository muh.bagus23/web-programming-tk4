@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(\Request::route()->getName() == 'admin.user.create')
                <div class="card-header">{{ __('Create User') }}</div>
                @else
                <div class="card-header">{{ __('Edit User '.$user->name) }}</div>
                @endif

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="m-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session('msg'))
                    <div class="alert alert-info">
                        {!! session('msg') !!}
                    </div>
                    @endif

                    @if(\Request::route()->getName() == 'admin.user.create')
                    <form method="POST" action="{{ route('admin.user.create') }}" enctype="multipart/form-data">
                    @else
                    <form method="POST" action="{{ route('admin.user.edit', $user->id) }}" enctype="multipart/form-data">
                        @method('PUT')
                    @endif

                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input type="text" required name="name" value="{{ old('name') ?? ($user->name ?? null) }}" class="form-control @error('name') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="birthdate" class="col-md-4 col-form-label text-md-end">{{ __('Birth Date') }}</label>

                            <div class="col-md-6">
                                <input type="date" required name="birthdate" value="{{ old('birthdate') ?? ($user->birthdate ?? null) }}" class="form-control @error('birthdate') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="address" class="col-md-4 col-form-label text-md-end">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <textarea name="address" class="form-control @error('address') is-invalid @enderror">{{ old('address') ?? ($user->address ?? null) }}</textarea>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="ktp" class="col-md-4 col-form-label text-md-end">{{ __('KTP') }}</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control @error('ktp_image_url') is-invalid @enderror" name="ktp_image_url" <?=(\Request::route()->getName() == 'admin.user.create' ? 'required' : null)?> accept="image/png, image/jpeg, image/jpg" />
                                <img src="{{ $user->ktp_image_url ?? null }}" class="img-fluid my-2" />
                            </div>
                        </div>

                        <hr>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input type="email" required name="email" value="{{ old('email') ?? ($user->email ?? null) }}" class="form-control @error('email') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="username" class="col-md-4 col-form-label text-md-end">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input type="text" required name="username" value="{{ old('username') ?? ($user->username ?? null) }}" class="form-control @error('username') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" <?=(\Request::route()->getName() == 'admin.user.create' ? 'required' : null)?> name="password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-end">{{ __('Retype Password') }}</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" <?=(\Request::route()->getName() == 'admin.user.create' ? 'required' : null)?> name="password_confirmation" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <a class="btn btn-secondary" href="{{ route('admin.user.index') }}">Cancel</a>
                                @if(\Request::route()->getName() == 'admin.user.create')
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                                @else
                                <button type="submit" class="btn btn-warning">
                                    {{ __('Update') }}
                                </button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection