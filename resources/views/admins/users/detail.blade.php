@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Detail User '.$user->name) }}</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="m-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session('msg'))
                    <div class="alert alert-info">
                        {{ session('msg') }}
                    </div>
                    @endif

                    <form method="POST" action="{{ route('admin.user.create') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input type="text" disabled required name="name" value="{{ $user->name ?? old('name') }}" class="form-control @error('name') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="birthdate" class="col-md-4 col-form-label text-md-end">{{ __('Birth Date') }}</label>

                            <div class="col-md-6">
                                <input type="date" disabled required name="birthdate" value="{{ $user->birthdate ?? old('birthdate') }}" class="form-control @error('birthdate') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="address" class="col-md-4 col-form-label text-md-end">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <textarea name="address" class="form-control @error('address') is-invalid @enderror">{{ $user->address ?? old('address') }}</textarea>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="ktp" class="col-md-4 col-form-label text-md-end">{{ __('KTP') }}</label>

                            <div class="col-md-6">
                                <img src="{{ $user->ktp_image_url }}" class="img-fluid" />
                            </div>
                        </div>

                        <hr>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input type="email" disabled required name="email" value="{{ $user->email ?? old('email') }}" class="form-control @error('email') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="username" class="col-md-4 col-form-label text-md-end">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input type="text" disabled required name="username" value="{{ $user->username ?? old('username') }}" class="form-control @error('username') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <a class="btn btn-secondary" href="{{ route('admin.user.index') }}">Cancel</a>
                                <a class="btn btn-warning" href="{{ route('admin.user.edit', $user->id) }}">Edit</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection