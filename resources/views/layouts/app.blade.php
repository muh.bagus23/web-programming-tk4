<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                @guest
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                @else
                    @if(auth()->user()->isUser())
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    @elseif(auth()->user()->isStaff())
                    <a class="navbar-brand" href="{{ url('/admin/user') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    @else
                    <a class="navbar-brand" href="{{ url('/admin') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    @endif
                @endguest
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                        @else
                            @switch(Auth::user()->role)
                                @case('admin')
                                    <li class="nav-item px-2">
                                        <a class="nav-link" href="{{ route('admin.order.index') }}">{{ __('Transaction') }}</a>
                                    </li>
                                    <li class="nav-item px-2">
                                        <a class="nav-link" href="{{ route('admin.product.index') }}">{{ __('Product') }}</a>
                                    </li>
                                    <li class="nav-item px-2">
                                        <a class="nav-link" href="{{ route('admin.staff.index') }}">{{ __('Staff') }}</a>
                                    </li>
                                    <li class="nav-item px-2" style="border-right: 1px solid #aaa;">
                                        <a class="nav-link" href="{{ route('admin.user.index') }}">{{ __('User') }}</a>
                                    </li>
                                    @break
                                @case('staff')
                                    <li class="nav-item px-2">
                                        <a class="nav-link" href="{{ route('admin.order.index') }}">{{ __('Transaction') }}</a>
                                    </li>
                                    <li class="nav-item px-2">
                                        <a class="nav-link" href="{{ route('admin.product.index') }}">{{ __('Product') }}</a>
                                    </li>
                                    <li class="nav-item px-2" style="border-right: 1px solid #aaa;">
                                        <a class="nav-link" href="{{ route('admin.user.index') }}">{{ __('User') }}</a>
                                    </li>
                                    @break
                                @case('user')
                                    <li class="nav-item px-2">
                                        <a class="nav-link" href="{{ route('order.index') }}">{{ __('Transaction') }}</a>
                                    </li>
                                    <li class="nav-item px-2" style="border-right: 1px solid #aaa;">
                                        <a class="nav-link" href="{{ route('order.cart') }}">{{ __('Cart') }}</a>
                                    </li>
                                    @break
                            @endswitch
                            <li class="nav-item dropdown ps-2">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    @yield('script')
</body>
</html>
